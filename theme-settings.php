<?php declare(strict_types = 1);

/**
 * @file
 * Theme settings form for diner_delights theme.
 */

use Drupal\Core\Form\FormState;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function diner_delights_form_system_theme_settings_alter(array &$form, FormState $form_state): void {

  $form['diner_delights'] = [
    '#type' => 'details',
    '#title' => t('Diner Delights'),
    '#open' => TRUE,
  ];
  
  //Home Banner section.
  $form['diner_delights']['banner_display'] = [
    '#type' => 'checkbox',
    '#title' => t('Show banner'),
    '#default_value' => theme_get_setting('banner_display', 'diner_delights'),
    '#description' => t("Check this option to show banner in front page.
    Uncheck to hide."),
  ];

  $form['diner_delights']['heading'] = [
    '#type' => 'textfield',
    '#title' => t('First Headling'),
    '#default_value' => theme_get_setting('heading', 'diner_delights'),
  ];

  $form['diner_delights']['description'] = [
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => theme_get_setting('description', 'diner_delights'),
  ];

  $form['diner_delights']['primary_button'] = [
    '#type' => 'textfield',
    '#title' => t('Primary Button'),
    '#default_value' => theme_get_setting('primary_button', 'diner_delights'),
  ];

  $form['diner_delights']['second_button'] = [
    '#type' => 'textfield',
    '#title' => t('Second Button'),
    '#default_value' => theme_get_setting('second_button', 'diner_delights'),
  ];

  $form['diner_delights']['youtube_url'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube Link'),
    '#default_value' => theme_get_setting('youtube_url', 'diner_delights'),
  ];

  // Footer contact details section.
  $form['footer_contact_details'] = [
    '#type' => 'details',
    '#title' => t('Footer Contact details'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  
  $form['footer_contact_details']['show_contact_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show contact details'),
    '#default_value' => theme_get_setting('show_contact_details', 'diner_delights'),
    '#description' => t("Show/Hide contact_details"),
  ];

  $form['footer_contact_details']['footer_logo'] = [
    '#type' => 'managed_file',
    '#title' => t('Footer Logo image'),
    '#default_value' => theme_get_setting('footer_logo', 'diner_delights'),
    '#upload_location' => 'public://',
    '#upload_validators' => [
      'file_validate_extensions' => ['gif png jpg jpeg svg'],
    ],
  ];

  $form['footer_contact_details']['location'] = [
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#default_value' => theme_get_setting('location', 'diner_delights'),
    '#description' => t("Text field for location"),
  ];

  $form['footer_contact_details']['contact'] = [
    '#type' => 'textfield',
    '#title' => t('Contact'),
    '#default_value' => theme_get_setting('contact', 'diner_delights'),
    '#description' => t("Text field for Contact"),
    '#maxlength' => 64,
    '#size' => 64,
  ];
 
  $form['footer_contact_details']['emails'] = [
    '#type' => 'email',
    '#title' => ('Email'),
    '#default_value' => theme_get_setting('emails', 'diner_delights'),
    '#description' => t("Enter your email"),
  ];

  // Footer Social media icons section.
  $form['social_icon'] = [
    '#type' => 'details',
    '#title' => t('Social Media Link'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social_icon']['show_social_icon'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('show_social_icon', 'diner_delights'),
    '#description' => t("Show/Hide social media links"),
  ];
  $form['social_icon']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook Link'),
    '#default_value' => theme_get_setting('facebook_url', 'diner_delights'),
  ];
  $form['social_icon']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter Link'),
    '#default_value' => theme_get_setting('twitter_url', 'diner_delights'),
  ];
  $form['social_icon']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram Link'),
    '#default_value' => theme_get_setting('instagram_url', 'diner_delights'),
  ];
  $form['social_icon']['skype'] = [
    '#type' => 'textfield',
    '#title' => t('Skype Link'),
    '#default_value' => theme_get_setting('skype', 'diner_delights'),
  ];
  $form['social_icon']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin Link'),
    '#default_value' => theme_get_setting('linkedin_url', 'diner_delights'),
  ];

  // Footer News letter details section.
   $form['footer_newsletter_details'] = [
    '#type' => 'details',
    '#title' => t('Footer newsletter details'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['footer_newsletter_details']['show_newsletter_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show contact details'),
    '#default_value' => theme_get_setting('show_newsletter_details', 'diner_delights'),
    '#description' => t("Show/Hide News_details"),
  ];

  $form['footer_newsletter_details']['footer_news_head'] = [
    '#type' => 'textfield',
    '#title' => t('Our Headline'),
    '#default_value' => theme_get_setting('footer_news_head', 'diner_delights'),
    '#description' => t("Text field for News heading"),
  ];

  $form['footer_newsletter_details']['footer_news_desc'] = [
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => theme_get_setting('footer_news_desc', 'diner_delights'),
    '#description' => t("Text field for news Description."),
  ];

  $form['footer_details'] = [
    '#type' => 'details',
    '#title' => t('Copyright'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  // Footer copyright section.
  $form['footer_details']['copyright']['show_copyright'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Copyright text'),
    '#default_value' => theme_get_setting('show_copyright', 'diner_delights'),
    '#description'   => t("Check this option to show Copyright text. Uncheck to hide."),
  ];

  $form['footer_details']['copyright']['copyright_text'] = [
    '#type' => 'textfield',
    '#title' => t('Enter copyright text'),
    '#default_value' => check_markup(theme_get_setting('copyright_text'), 'full_html', 'diner_delights'),
  ];

  $form['footer_details']['copyright']['copyright_deve_text'] = [
    '#type' => 'textarea',
    '#title' => t('Developer info'),
    '#default_value' => theme_get_setting('copyright_deve_text', 'diner_delights'),
    '#description' => t("Developer info."),
  ];

  // Footer arrow left/right section.
  $form['arrow_section'] = [
    '#type' => 'details',
    '#title' => t('Show Scroll to top arrow'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['arrow_section']['show_scroller'] = [
    '#type' => 'checkbox',
    '#title' => t('Show scroller'),
    '#default_value' => theme_get_setting('show_scroller', 'diner_delights'),
    '#description'   => t("Check this option to show scroller Uncheck to hide."),
  ];

  $options_theme = [
    'left' => 'Left Side',
    'right' => 'Right Side',
  ];
  $form['arrow_section']['arrow_section']['arrow_up'] = [
    '#type' => 'select',
    '#title' => t('Arrow Up Left/Right:'),
    '#default_value' => theme_get_setting('arrow_up', 'diner_delights'),
    '#description' => t("Choose Arrow Up Icon Position"),
    '#options' => $options_theme,
  ];
}
